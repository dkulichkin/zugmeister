import { connect } from 'react-redux';
import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { StyleSheet, View, Text, FlatList, Image } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  row: {
    flex: 1,
    padding: 15,
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  image: {
    marginRight: 10,
    width: 50,
    height: 50,
    borderRadius: 25
  },
  colorsList: {
    flexDirection: 'row'
  },
  brand: {
    marginRight: 10
  },
  colorIcon: {
    marginRight: 10,
    width: 10,
    height: 10,
    borderRadius: 5,
    borderColor: '#ccc',
    borderWidth: 1
  }
});

class VehiclesList extends Component {

  static get propTypes() {
    return {
      filtered: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        type: PropTypes.string.isRequired,
        brand: PropTypes.string.isRequired,
        colors: PropTypes.arrayOf(PropTypes.string).isRequired,
        img: PropTypes.string.isRequired
      })).isRequired
    };
  }

  renderItem({item}) {
    return (
      <View style={styles.row}>
        <Image source={{ uri: item.img }} style={styles.image} resizeMode="cover" />
        <Text style={styles.brand}>{item.brand}</Text>
        <View style={styles.colorsList}>
          { item.colors.map((color, i) => <View style={[styles.colorIcon, { backgroundColor: color }]} key={i} /> ) }
        </View>
      </View>
    )
  }

  render() {
    const { filtered } = this.props;

    return (
      <FlatList
        style={styles.container}
        data={filtered}
        renderItem={this.renderItem}
        keyExtractor={({id}) => id}
      />
    );
  }
}


const mapStateToProps = (state) => {
  const { filtered } = state;
  return { filtered };
};

export default connect(mapStateToProps)(VehiclesList);
