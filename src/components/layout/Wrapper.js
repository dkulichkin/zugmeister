import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { StyleSheet, ActivityIndicator, View, Alert } from 'react-native';
import { loadData } from '../../actions';
import Form from './Form';
import VehiclesList from './VehiclesList';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column'
  }
});

class Wrapper extends Component {

  static get propTypes() {
    return {
      dataLoaded: PropTypes.bool.isRequired,
      dataLoading: PropTypes.bool.isRequired,
      requestError: PropTypes.string,
      loadData: PropTypes.func.isRequired
    };
  }

  componentDidMount() {
    const { dataLoaded, loadData } = this.props;
    if (!dataLoaded) {
      loadData();
    }
  }

  render() {
    const { dataLoaded, dataLoading, requestError } = this.props;

    return (
      <View style={styles.container}>
        { dataLoading && <ActivityIndicator size="large" /> }
        { requestError && Alert.show(requestError) }
        { dataLoaded && <Form /> }
        { dataLoaded && <VehiclesList /> }
      </View>
    );
  }
};

const mapStateToProps = (state) => {
  const { dataLoaded, dataLoading, requestError } = state;
  return { dataLoaded, dataLoading, requestError };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({ loadData }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Wrapper);
