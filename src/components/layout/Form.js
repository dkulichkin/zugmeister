import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { StyleSheet, View } from 'react-native';
import ModalPicker from 'react-native-modal-picker';
import { selectBrand, selectType, selectColor } from '../../actions';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingTop: 40,
    paddingBottom: 20
  },
  picker: {
    borderWidth: 0,
    backgroundColor: '#841584'
  },
  selectText: {
    color: 'white'
  }
});

class Form extends Component {

  static get propTypes() {
    return {
      lists: PropTypes.shape({
        types: PropTypes.arrayOf(PropTypes.shape({
          key: PropTypes.string.isRequired,
          label: PropTypes.string.isRequired })
        ).isRequired,
        brands: PropTypes.arrayOf(PropTypes.shape({
          key: PropTypes.string.isRequired,
          label: PropTypes.string.isRequired })
        ).isRequired,
        colors: PropTypes.arrayOf(PropTypes.shape({
          key: PropTypes.string.isRequired,
          label: PropTypes.string.isRequired })
        ).isRequired
      }).isRequired,
      selected: PropTypes.shape({
        type: PropTypes.string.isRequired,
        brand: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired
      }).isRequired,
      selectBrand: PropTypes.func.isRequired,
      selectType: PropTypes.func.isRequired,
      selectColor: PropTypes.func.isRequired
    };
  }

  // Android always calls onChange regardless of value on re-render
  onTypeChange(val) {
    const { selected: { type } } = this.props;
    if (val !== type) {
      this.props.selectType(val);
    }
  }

  onBrandChange(val) {
    const { selected: { brand } } = this.props;
    if (val !== brand) {
      this.props.selectBrand(val);
    }
  }

  onColorChange(val) {
    const { selected: { color } } = this.props;
    if (val !== color) {
      this.props.selectColor(val);
    }
  }

  render() {
    const {
      lists: { types, brands, colors },
      selected: { type, brand, color }
    } = this.props;

    const selectedTypeLabel = types.find((item) => item.key === type)['label'];
    const selectedBrandLabel = brands.find((item) => item.key === brand)['label'];
    const selectedColorLabel = colors.find((item) => item.key === color)['label'];

    return (
      <View style={styles.container}>
        <ModalPicker
          selectStyle={styles.picker}
          selectTextStyle={styles.selectText}
          data={types}
          onChange={(option) => this.onTypeChange(option.key)}
          initValue={selectedTypeLabel}
        />
        <ModalPicker
          selectStyle={styles.picker}
          selectTextStyle={styles.selectText}
          data={brands}
          onChange={(option) => this.onBrandChange(option.key)}
          initValue={selectedBrandLabel}
        />
        <ModalPicker
          selectStyle={styles.picker}
          selectTextStyle={styles.selectText}
          data={colors}
          onChange={(option) => this.onColorChange(option.key)}
          initValue={selectedColorLabel}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { lists, selected } = state;
  return { lists, selected };
};

const mapDispatchToProps = (dispatch) => bindActionCreators({ selectBrand, selectType, selectColor }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Form);
