import update from 'immutability-helper';
import {
  SELECT_TYPE, SELECT_BRAND, SELECT_COLOR,
  LOAD_DATA_REQUEST, LOAD_DATA_SUCCESS, LOAD_DATA_ERROR } from '../actions';

const ALL_TYPES = 'All types';
const ALL_BRANDS = 'All brands';
const ALL_COLORS = 'All colors';

const initialState = {
  dataLoaded: false,
  dataLoading: false,
  lists: {},
  selected: {
    type: '',
    brand: '',
    color: ''
  },
  requestError: null
};

// Main business logic helper populating dropdowns data (lists: { ... })
export const getListFromDataByFieldAndFilter = (data, field, filters) => {
  function isIncluded (model, filters) {
    if (!filters) return true;
    const { type, brand, color } = filters;
    if (type && field !== 'type' && model.type !== type) return false;
    if (brand && field !== 'brand' && model.brand !== brand) return false;
    if (color && field !== 'colors' && !model.colors.includes(color)) return false;
    return true;
  }

  const result = data.reduce((res, current) => {
    if (isIncluded(current, filters)) {
      Array.isArray(current[field])
        ? current[field].forEach((el) => res.add(el)) // for the colors field
        : res.add(current[field]);
    }
    return res;
  }, new Set());

  return Array.from(result).sort();
};

// Filtering helper for getting list by selected values
const getFilteredData = (data, { type, brand, color }) => {
  return data.filter(model => {
    if (type && model.type !== type) return false;
    if (brand && model.brand !== brand) return false;
    if (color && !model.colors.includes(color)) return false;
    return true;
  }).sort((a, b) => {
    if(a.brand < b.brand) return -1;
    if(a.brand > b.brand) return 1;
    return 0;
  })
};

// A function for transforming data for list component's expected format
// adds also reset option
const transformIntoListOptions = (data, allKindsLabel) => {
  return [{ key: '', label: allKindsLabel }]
    .concat(data.map((item) => ({ key: item, label: item })));
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_DATA_REQUEST: {
      return update(state, {
        dataLoading: { $set: true },
        requestError: { $set: null },
        $unset: ['data'],
        lists: { $unset: ['types', 'brands', 'colors'] },
        selected: {
          type: { $set: initialState.selected.type },
          brand: { $set: initialState.selected.brand },
          color: { $set: initialState.selected.color }
        }
      });
    }

    case LOAD_DATA_SUCCESS: {
      const data = action.result;
      const types = getListFromDataByFieldAndFilter(data, 'type');
      const brands = getListFromDataByFieldAndFilter(data, 'brand');
      const colors = getListFromDataByFieldAndFilter(data, 'colors');

      return update(state, {
        dataLoading: { $set: false },
        dataLoaded: { $set: true },
        data: { $set: data },
        filtered: { $set: getFilteredData(data, state.selected) },
        lists: {
          types: { $set: transformIntoListOptions(types, ALL_TYPES) },
          brands: { $set: transformIntoListOptions(brands, ALL_BRANDS) },
          colors: { $set: transformIntoListOptions(colors, ALL_COLORS) }
        }
      });
    }

    case LOAD_DATA_ERROR: {
      return update(state, {
        dataLoading: { $set: false },
        requestError: { $set: action.error }
      });
    }

    case SELECT_TYPE: {
      const selectedType = action.result;
      Object.assign(state.selected, { type: selectedType });

      const data = state.data;
      const brands = getListFromDataByFieldAndFilter(data, 'brand', state.selected);
      const colors = getListFromDataByFieldAndFilter(data, 'colors', state.selected);
      const selectedBrand = brands.includes(state.selected.brand) ? state.selected.brand : initialState.selected.brand;
      const selectedColor = colors.includes(state.selected.color) ? state.selected.color : initialState.selected.color;

      return update(state, {
        selected: {
          brand: { $set: selectedBrand },
          color: { $set: selectedColor }
        },
        filtered: { $set: getFilteredData(data, { type: selectedType, brand: selectedBrand, color: selectedColor }) },
        lists: {
          brands: { $set: transformIntoListOptions(brands, ALL_BRANDS) },
          colors: { $set: transformIntoListOptions(colors, ALL_COLORS) }
        }
      });
    }

    case SELECT_BRAND: {
      const selectedBrand = action.result;
      Object.assign(state.selected, { brand: selectedBrand });

      const data = state.data;
      const types = getListFromDataByFieldAndFilter(data, 'type', state.selected);
      const colors = getListFromDataByFieldAndFilter(data, 'colors', state.selected);
      const selectedType = types.includes(state.selected.type) ? state.selected.type : initialState.selected.type;
      const selectedColor = colors.includes(state.selected.color) ? state.selected.color : initialState.selected.color;

      return update(state, {
        selected: {
          type: { $set: selectedType },
          color: { $set: selectedColor }
        },
        filtered: { $set: getFilteredData(data, { type: selectedType, brand: selectedBrand, color: selectedColor }) },
        lists: {
          types: { $set: transformIntoListOptions(types, ALL_TYPES) },
          colors: { $set: transformIntoListOptions(colors, ALL_COLORS) }
        }
      });
    }

    case SELECT_COLOR: {
      const selectedColor = action.result;
      Object.assign(state.selected, { color: selectedColor });

      const data = state.data;
      const types = getListFromDataByFieldAndFilter(data, 'type', state.selected);
      const brands = getListFromDataByFieldAndFilter(data, 'brand', state.selected);
      const selectedType = types.includes(state.selected.type) ? state.selected.type : initialState.selected.type;
      const selectedBrand = brands.includes(state.selected.brand) ? state.selected.brand : initialState.selected.brand;

      return update(state, {
        selected: {
          type: { $set: selectedType },
          brand: { $set: selectedBrand }
        },
        filtered: { $set: getFilteredData(data, { type: selectedType, brand: selectedBrand, color: selectedColor }) },
        lists: {
          types: { $set: transformIntoListOptions(types, ALL_TYPES) },
          brands: { $set: transformIntoListOptions(brands, ALL_BRANDS) }
        }
      });
    }

    default:
      return state;
  }
};

export default rootReducer;
