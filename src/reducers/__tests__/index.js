import { getListFromDataByFieldAndFilter } from '../index';

const mockData = [
  {
    id: 1,
    type: 'car',
    brand: 'Bugatti Veyron',
    colors: ['red', 'black'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg/520px-Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg'
  },
  {
    id: 2,
    type: 'airplane',
    brand: 'Boeing 787 Dreamliner',
    colors: ['red', 'white', 'black', 'green'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg/600px-All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg'
  },
  {
    id: 3,
    type: 'train',
    brand: 'USRA 0-6-6',
    colors: ['yellow', 'white', 'black'],
    img: 'https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/UP_4466_Neil916.JPG/600px-UP_4466_Neil916.JPG'
  },
  {
    id: 4,
    type: 'airplane',
    brand: 'Canadair North Star',
    colors: ['red', 'blue', 'yellow', 'green'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/BOAC_C-4_Argonaut_Heathrow_1954.jpg/600px-BOAC_C-4_Argonaut_Heathrow_1954.jpg'
  },
  {
    id: 5,
    type: 'airplane',
    brand: 'Airbus A400M Atlas',
    colors: ['red', 'white'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/A400M-1969.jpg/600px-A400M-1969.jpg'
  },
  {
    id: 6,
    type: 'airplane',
    brand: 'Bloch MB.131',
    colors: ['yellow', 'blue', 'brown'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/e/e5/Bloch_MB_131_San_Diego_Air_%26_Space_Museum_3.jpg'
  },
  {
    id: 7,
    type: 'train',
    brand: 'Prairie 2-6-2',
    colors: ['red', 'white', 'grey'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/CFR_Steam_locomotive.jpg/600px-CFR_Steam_locomotive.jpg'
  },
  {
    id: 8,
    type: 'train',
    brand: 'EMD GP40',
    colors: ['black', 'grey', 'white'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/EMD_GP40_B%26M_339_Wells_Maine.jpg/600px-EMD_GP40_B%26M_339_Wells_Maine.jpg'
  },
  {
    id: 9,
    type: 'train',
    brand: 'Amer 4-4-0',
    colors: ['red', 'black'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/440woodcut.jpg/600px-440woodcut.jpg'
  },
  {
    id: 10,
    type: 'car',
    brand: 'Ferrari F40',
    colors: ['red', 'yellow'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/c/cb/F40_Ferrari_20090509.jpg'
  },
  {
    id: 11,
    type: 'car',
    brand: 'Lamborghini Huracán',
    colors: ['black', 'white'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/2014-03-04_Geneva_Motor_Show_1379.JPG/440px-2014-03-04_Geneva_Motor_Show_1379.JPG'
  },
  {
    id: 12,
    type: 'car',
    brand: 'Porsche Carrera GT',
    colors: ['green', 'yellow'],
    img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Porsche_Carrera_GT_-_Goodwood_Breakfast_Club_%28July_2008%29.jpg/440px-Porsche_Carrera_GT_-_Goodwood_Breakfast_Club_%28July_2008%29.jpg'
  }
];
const typesList = ['airplane', 'car', 'train'];
const brandsList = ['Airbus A400M Atlas', 'Amer 4-4-0', 'Bloch MB.131', 'Boeing 787 Dreamliner', 'Bugatti Veyron', 'Canadair North Star', 'EMD GP40', 'Ferrari F40', 'Lamborghini Huracán', 'Porsche Carrera GT', 'Prairie 2-6-2', 'USRA 0-6-6'];
const colorsList = ['black', 'blue', 'brown', 'green', 'grey', 'red', 'white', 'yellow'];


describe('RootReducer', () => {

  describe('Main business logic helper getListFromDataByFieldAndFilter', () => {

    it('should return all sorted type items when called w/o filters and with default values', () => {
      const filters = { type: '', brand: '', color: '' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type');
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand');
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors');
      const res4 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res5 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res6 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(typesList);
      expect(res2).toEqual(brandsList);
      expect(res3).toEqual(colorsList);
      expect(res4).toEqual(typesList);
      expect(res5).toEqual(brandsList);
      expect(res6).toEqual(colorsList);
    });

    it('should return correct values filtered by { type: ..., brand: \'\', color: \'\' }', () => {
      const filters = { type: 'airplane', brand: '', color: '' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(typesList);
      expect(res2).toEqual(['Airbus A400M Atlas', 'Bloch MB.131', 'Boeing 787 Dreamliner', 'Canadair North Star']);
      expect(res3).toEqual(['black', 'blue', 'brown', 'green', 'red', 'white', 'yellow']);
    });

    it('should return correct values filtered by { type: \'\', brand: ..., color: \'\' }', () => {
      const filters = { type: '', brand: 'Bugatti Veyron', color: '' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(['car']);
      expect(res2).toEqual(brandsList);
      expect(res3).toEqual(['black', 'red']);
    });

    it('should return correct values filtered by { type: \'\', brand: \'\', color: ... }', () => {
      const filters = { type: '', brand: '', color: 'green' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(['airplane', 'car']);
      expect(res2).toEqual(['Boeing 787 Dreamliner', 'Canadair North Star', 'Porsche Carrera GT']);
      expect(res3).toEqual(colorsList);
    });

    it('should return correct values filtered by { type: ..., brand: \'\', color: ... }', () => {
      const filters = { type: 'car', brand: '', color: 'red' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(typesList);
      expect(res2).toEqual(['Bugatti Veyron', 'Ferrari F40']);
      expect(res3).toEqual(['black', 'green', 'red', 'white', 'yellow']); // all colors among the cars
    });

    it('should return correct values filtered by { type: ..., brand: ..., color: \'\' }', () => {
      const filters = { type: 'car', brand: 'Bugatti Veyron', color: '' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(['car']);
      expect(res2).toEqual(['Bugatti Veyron', 'Ferrari F40', 'Lamborghini Huracán', 'Porsche Carrera GT']); // all car brands
      expect(res3).toEqual(['black', 'red']);
    });

    it('should return correct values filtered by { type: \'\', brand: ..., color: ... }', () => {
      const filters = { type: '', brand: 'Bugatti Veyron', color: 'red' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(['car']);
      expect(res2).toEqual(['Airbus A400M Atlas', 'Amer 4-4-0', 'Boeing 787 Dreamliner', 'Bugatti Veyron', 'Canadair North Star', 'Ferrari F40', 'Prairie 2-6-2']); // all red vehicles
      expect(res3).toEqual(['black', 'red']);
    });

    it('should return correct values filtered by { type: ..., brand: ..., color: ... }', () => {
      const filters = { type: 'airplane', brand: 'Airbus A400M Atlas', color: 'red' };
      const res1 = getListFromDataByFieldAndFilter(mockData, 'type', filters);
      const res2 = getListFromDataByFieldAndFilter(mockData, 'brand', filters);
      const res3 = getListFromDataByFieldAndFilter(mockData, 'colors', filters);
      expect(res1).toEqual(['airplane']);
      expect(res2).toEqual(['Airbus A400M Atlas', 'Boeing 787 Dreamliner', 'Canadair North Star']); // all red aircrafts
      expect(res3).toEqual(['red', 'white']); // colors of Airbus A400M Atlas
    });


  });

});
