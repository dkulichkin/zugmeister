import trafficMeister from '../service';

export const LOAD_DATA_REQUEST = 'LOAD_DATA_REQUEST';
export const LOAD_DATA_SUCCESS = 'LOAD_DATA_SUCCESS';
export const LOAD_DATA_ERROR = 'LOAD_DATA_ERROR';

export const SELECT_TYPE = 'SELECT_TYPE';
export const SELECT_BRAND = 'SELECT_BRAND';
export const SELECT_COLOR = 'SELECT_COLOR';

export const loadData = () => (dispatch) => {
  dispatch({ type: LOAD_DATA_REQUEST });
  trafficMeister.fetchData((err, data) => (err
    ? dispatch({ type: LOAD_DATA_ERROR, err })
    : dispatch({ type: LOAD_DATA_SUCCESS, result: data })
  ));
};

export const selectType = type => ({ type: SELECT_TYPE, result: type });

export const selectBrand = brand => ({ type: SELECT_BRAND, result: brand });

export const selectColor = color => ({ type: SELECT_COLOR, result: color });
